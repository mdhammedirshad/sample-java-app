# sample-java-app
This is Sample Java Application related to Devops basic training

##tool required to build and package

1. java 1.8+
2. Maven 3.5+
3. git


##Topics to discuss further
1. Squash merge
2. SonarQube Integration
3. Install Jenkins
4. UCD Deploy
